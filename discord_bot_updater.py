
"""
https://discord.com/developers/docs/interactions/slash-commands#registering-a-command
"""

import requests

APPLICATION_ID = ""
GUILD_ID = ""
BOT_TOKEN = ""

url = f"https://discord.com/api/v8/applications/{APPLICATION_ID}/guilds/{GUILD_ID}/commands"
update_url = f"https://discord.com/api/v8/applications/{APPLICATION_ID}/guilds/{GUILD_ID}/commands/"

gs_cid = ""
gs_json = {
        "name": "gameserver",
        "description": "Control the gameservers hosted on AWS",
        "options": [
            {
                "name": "valheim",
                "description": "Control the Valheim ECS Cluster",
                "type": 2,
                "options": [
                    {
                        "name": "start",
                        "description": "Start the server",
                        "type": 1
                    },
                    {
                        "name": "stop",
                        "description": "Stop the server",
                        "type": 1
                    },
                    {
                        "name": "status",
                        "description": "Show the server status",
                        "type": 1
                    }
                ]
            },
            {
                "name": "satisfactory",
                "description": "Control the Satisfactory ECS Cluster",
                "type": 2,
                "options": [
                    {
                        "name": "start",
                        "description": "Start the server",
                        "type": 1
                    },
                    {
                        "name": "stop",
                        "description": "Stop the server",
                        "type": 1
                    },
                    {
                        "name": "status",
                        "description": "Show the server status",
                        "type": 1
                    }
                ]
            }
        ]
}

test_cid = ""
test_json = {
        "name": "test",
        "description": "Test the bot"
}

headers = {
    "Authorization": f"Bot {BOT_TOKEN}"
}

if __name__ == "__main__":
    r = requests.patch(update_url+gs_cid, headers=headers, json=gs_json)
    print(r.content)
    # r = requests.patch(update_url+test_cid, headers=headers, json=test_json)
    # print(r.content)
