from nacl.signing import VerifyKey
from nacl.exceptions import BadSignatureError
import json
import boto3
client = boto3.client('ecs')

# Your public key can be found on your application in the Developer Portal
PUBLIC_KEY = ""

srv_srvc_names = {
    "satisfactory": "satisfactoryServerService",
    "valheim": "valheimServerService"
}

srv_cluster_arns = {
    "satisfactory": "",
    "valheim": ""
}


def lambda_handler(event, context):
    try:
        body = json.loads(event['body'])
        print(json.dumps(body))
        signature = event['headers']['x-signature-ed25519']
        timestamp = event['headers']['x-signature-timestamp']
        verify_key = VerifyKey(bytes.fromhex(PUBLIC_KEY))
        message = timestamp + json.dumps(body, separators=(',', ':'))
        try:
            verify_key.verify(message.encode(), signature=bytes.fromhex(signature))
        except BadSignatureError:
            return {
                'statusCode': 401,
                'body': json.dumps('invalid request signature')
            }

        t = body['type']
        if t == 1:
            return {
                'statusCode': 200,
                'body': json.dumps({
                    'type': 1
                })
            }
        elif t == 2:
            return command_handler(body)
        else:
            return {
                'statusCode': 400,
                'body': json.dumps('unhandled request type')
            }
    except Exception as e:
        json.dumps(e)
        raise


def command_handler(body):
    command = body['data']['name']
    if command == 'test':
        return {
            'statusCode': 200,
            'body': json.dumps({
                'type': 4,
                'data': {
                    'content': 'Hello, World.',
                }
            })
        }
    elif command == 'gameserver':
        try:
            options = body['data']['options'][0]
            svr_target = options['name']
            svr_commd = options['options'][0]['name']
            resp_str = f"{svr_commd}ing {svr_target}"
            if svr_commd == "status":
                try:
                    resp = client.describe_services(
                        cluster=srv_cluster_arns[svr_target],
                        services=[srv_srvc_names[svr_target], ]
                    )
                    resp2 = client.list_tasks(
                        cluster=srv_cluster_arns[svr_target],
                        desiredStatus='RUNNING'
                    )
                    ips = []
                    print("Getting Public IPs")
                    if len(resp2['taskArns']) != 0:
                        resp3 = client.describe_tasks(
                            cluster=srv_cluster_arns[svr_target],
                            tasks=resp2['taskArns']
                        )
                        print("Described tasks")
                        enis = []
                        for task in resp3['tasks']:
                            for attachment in task["attachments"]:
                                for detail in attachment["details"]:
                                    if detail["name"] == "networkInterfaceId":
                                        enis.append(detail["value"])
                        print("got ENIs")
                        for eni in enis:
                            eni_resource = boto3.resource("ec2").NetworkInterface(eni)
                            ips.append(eni_resource.association_attribute.get("PublicIp"))
                        print("got IPs")
                    desired_count = resp["services"][0]["desiredCount"]
                    running_count = resp["services"][0]["runningCount"]
                    pending_count = resp["services"][0]["pendingCount"]
                    print("got service counts")
                    resp_str = f"{svr_target} Status - Desired: {desired_count} | Running: {running_count}" \
                               f" | Pending: {pending_count}"
                    print("set resp_str")
                    if len(ips) != 0:
                        print("appending IP list")
                        resp_str += f" - Attached IPs: {str(ips)}"

                except Exception as e:
                    print(e)
                    resp_str = "Could not assemble status details"
                    
            elif svr_commd == "start":
                try:
                    resp = client.update_service(
                        cluster=srv_cluster_arns[svr_target],
                        service=srv_srvc_names[svr_target],
                        desiredCount=1
                    )

                    resp_str = f"Starting: {svr_target}"
                except Exception as e:
                    print(e)
                    resp_str = "Could not get server status"
            elif svr_commd == "stop":
                try:
                    resp = client.update_service(
                        cluster=srv_cluster_arns[svr_target],
                        service=srv_srvc_names[svr_target],
                        desiredCount=0
                    )

                    resp_str = f"Stopping: {svr_target}"
                except Exception as e:
                    print(e)
                    resp_str = "Could not get server status"
            return {
                'statusCode': 200,
                'body': json.dumps({
                    'type': 4,
                    'data': {
                        'content': resp_str,
                    }
                })
            }
        except Exception as e:
            print(e)
            print("Broken at subcommand...exit!")
            exit()

    else:
        return {
            'statusCode': 400,
            'body': json.dumps('unhandled command')
        }
